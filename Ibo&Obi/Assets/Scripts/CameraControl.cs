using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private Vector3 playerOffset;
    [SerializeField] private float minX;
    [SerializeField] private float maxX;
    [SerializeField] [Range(0.0f, 1.0f)] private float interpolate;
    

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 position = transform.position;
        Vector3 playerPosition = player.position;

        position = new Vector3(playerPosition.x,
                               playerPosition.y,
                                 position.z);

        position += playerOffset;
        
        if (position.x <= minX)
        {
            position.x = minX;
        }
        else if (position.x >= maxX)
        {
            position.x = maxX;
        }
        
        position = Vector3.Lerp(transform.position, position, interpolate);

        position = new Vector3(Mathf.Round(position.x * 8) / 8,
            Mathf.Round(position.y * 8) / 8,
            position.z);

        transform.position = position;
    }
}
