using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lever : MonoBehaviour
{
    [SerializeField] private float timeToOpen;

    [Space]
    
    [SerializeField] private BreakableTileMap _breakableTileMap;
    [SerializeField] private Vector2[] coordsOfTileToOpen;
    
    
    private bool _isOpened = false;

    public void Interact()
    {
        if (!_isOpened)
        {
            _isOpened = true;
            GetComponent<Animator>().enabled = true;
            StartCoroutine(WaitToOpen());
        }
    }
    
    IEnumerator WaitToOpen()
    {
        yield return new WaitForSeconds(timeToOpen);
        GetComponent<Animator>().enabled = false;

        foreach (var point in coordsOfTileToOpen)
        {
            _breakableTileMap.DestroyTile(point.x, point.y, false);
        }
    }
}
