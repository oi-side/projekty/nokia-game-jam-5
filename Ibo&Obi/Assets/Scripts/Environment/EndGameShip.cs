using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Timeline;

public class EndGameShip : MonoBehaviour
{
    [SerializeField] private float waitSeconds;
    [SerializeField] private float walkSpeed;
    [SerializeField] private float shipStartDelay;
    [SerializeField] private float timeBeforeEndingLevel;

    [SerializeField] private AudioClip endMusic;
    
    [SerializeField] private Vector2 iboPlayerShipPosition;
    [SerializeField] private Vector2 obiPlayerShipPosition;

    private PlayerController _playerController;
    private Rigidbody2D _playerRigidbody;
    private bool _triggered;
    private Animator _shipAnimator;
    private bool _isPlayerInPlace;

    
    // Start is called before the first frame update
    void Start()
    {
        _shipAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_isPlayerInPlace) return;
        
        if (_triggered)
        {
            _playerController.transform.parent = transform;
            if (_playerController.GetIsIboActive())
            {
                if (Mathf.Abs(_playerController.transform.localPosition.x - iboPlayerShipPosition.x) < 0.05f)
                {
                    if (Mathf.Abs(_playerController.transform.localPosition.y - iboPlayerShipPosition.y) < 0.05f)
                    {
                        _playerController.transform.localPosition = iboPlayerShipPosition;
                        _playerRigidbody.isKinematic = true;
                        _isPlayerInPlace = true;
                        _playerRigidbody.velocity = Vector2.zero;
                        StartCoroutine(WaitToStartShip());
                    }
                    else
                    {
                        _playerRigidbody.velocity = walkSpeed * Vector2.up;
                    }
                }

            }
            else
            {
                if (Mathf.Abs(_playerController.transform.localPosition.x - obiPlayerShipPosition.x) < 0.05f)
                {
                    if (Mathf.Abs(_playerController.transform.localPosition.y - obiPlayerShipPosition.y) < 0.05f)
                    {
                        _playerController.transform.localPosition = obiPlayerShipPosition;
                        _playerRigidbody.isKinematic = true;
                        _isPlayerInPlace = true;
                        _playerRigidbody.velocity = Vector2.zero;
                        StartCoroutine(WaitToStartShip());
                    }
                    else
                    {
                        _playerRigidbody.velocity = walkSpeed * Vector2.up;
                    }
                }
            }
        }
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (_triggered) return;
        
        if (other.CompareTag("Obi") || other.CompareTag("Ibo"))
        {
            _triggered = true;
            _playerController = other.transform.parent.GetComponent<PlayerController>();
            _playerController.SetCanUseInput(false);
            _playerRigidbody = other.transform.parent.GetComponent<Rigidbody2D>();
            _playerRigidbody.velocity = Vector2.zero;
            _playerController.SetMusic(endMusic);
            StartCoroutine(WaitForPlayerToFall());
        }
    }

    IEnumerator WaitForPlayerToFall()
    {
        yield return new WaitForSeconds(waitSeconds);
        _playerRigidbody.velocity = Vector2.right * walkSpeed;
        StartCoroutine(TimeToEndLevel());
    }
    
    IEnumerator TimeToEndLevel()
    {
        yield return new WaitForSeconds(timeBeforeEndingLevel);
        SceneManager.LoadScene(0);
    }

    IEnumerator WaitToStartShip()
    {
        yield return new WaitForSeconds(shipStartDelay);
        Camera.main.GetComponent<CameraControl>().enabled = false;
        _shipAnimator.SetTrigger("StartTrigger");
    }
    
}
