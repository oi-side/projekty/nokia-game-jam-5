using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{
    [SerializeField] private SpriteRenderer leftButton;
    [SerializeField] private SpriteRenderer rightButton;
    [SerializeField] private Transform movablePart;

    [Space]

    [SerializeField] private float timeToOpen;
    [SerializeField] private Sprite leftPressedSprite;
    [SerializeField] private Sprite rightPressedSprite;
    [SerializeField] private float offsetToMoveY;

    [Space]
    
    [SerializeField] private BreakableTileMap _breakableTileMap;
    [SerializeField] private Vector2[] coordsOfTileToOpen;
    
    private bool _isOpened = false;
    private bool _isPlayerStanding = false;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (_isOpened) return;
        if (other.CompareTag("Obi"))
        {
            _isPlayerStanding = true;
            StartCoroutine(WaitToOpen());
        }
        else if (other.CompareTag("Ibo"))
        {
            other.transform.parent.GetComponent<PlayerController>().IboNope();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (_isOpened) return;
        if (other.CompareTag("Obi"))
        {
            _isPlayerStanding = false;
        }
    }

    IEnumerator WaitToOpen()
    {
        yield return new WaitForSeconds(timeToOpen);
        if (_isPlayerStanding)
        {
            _isOpened = true;
            leftButton.sprite = leftPressedSprite;
            rightButton.sprite = rightPressedSprite;
            movablePart.position += Vector3.up * offsetToMoveY;

            foreach (var point in coordsOfTileToOpen)
            {
                _breakableTileMap.DestroyTile(point.x, point.y, false);
            }
        }
    }
}
