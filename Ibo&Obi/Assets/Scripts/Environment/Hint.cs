using System;
using System.Collections;
using UnityEngine;

public class Hint : MonoBehaviour
{
    [SerializeField] private SpriteRenderer hintSprite;

    private void Start()
    {
        hintSprite.enabled = false;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Obi") || other.CompareTag("Ibo"))
        {
            hintSprite.enabled = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Obi") || other.CompareTag("Ibo"))
        {
            hintSprite.enabled = false;
        }
    }
}
