using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndLevel : MonoBehaviour
{
    [SerializeField] private float waitSeconds;
    [SerializeField] private float walkSpeed;
    [SerializeField] private float timeBeforeEndingLevel;
    
    [SerializeField] private float iboShipPartOffset = 1.0f;
    [SerializeField] private float obiShipPartOffset = 1.6f;

    [SerializeField] private AudioClip endMusic;

    private PlayerController _playerController;
    private Rigidbody2D _playerRigidbody;
    private bool _triggered;

    private void Update()
    {
        if (_triggered)
        {
            if (_playerController.GetIsIboActive())
            {
                if (_playerController.transform.position.x >= transform.position.x + iboShipPartOffset)
                {
                    transform.parent = _playerController.transform;
                    Vector3 position = transform.localPosition;
                    position.x = -iboShipPartOffset;
                    transform.localPosition = position;
                }
            }
            
            else
            {
                if (_playerController.transform.position.x >= transform.position.x + obiShipPartOffset)
                {
                    transform.parent = _playerController.transform;
                    Vector3 position = transform.localPosition;
                    position.x = -obiShipPartOffset;
                    transform.localPosition = position;
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (_triggered) return;
        
        if (other.CompareTag("Obi") || other.CompareTag("Ibo"))
        {
            _triggered = true;
            _playerController = other.transform.parent.GetComponent<PlayerController>();
            _playerController.SetCanUseInput(false);
            _playerRigidbody = other.transform.parent.GetComponent<Rigidbody2D>();
            _playerRigidbody.velocity = Vector2.zero;
            _playerController.SetMusic(endMusic);
            StartCoroutine(WaitForPlayerToFall());
        }
    }

    IEnumerator WaitForPlayerToFall()
    {
        yield return new WaitForSeconds(waitSeconds);
        _playerRigidbody.velocity = Vector2.right * walkSpeed;
        StartCoroutine(TimeToEndLevel());
    }
    
    IEnumerator TimeToEndLevel()
    {
        yield return new WaitForSeconds(timeBeforeEndingLevel);
        int nextLevelIdx = SceneManager.GetActiveScene().buildIndex + 1;
        SceneManager.LoadScene(nextLevelIdx);
        PlayerPrefs.SetInt("AtLevelIdx", nextLevelIdx);
    }
}
