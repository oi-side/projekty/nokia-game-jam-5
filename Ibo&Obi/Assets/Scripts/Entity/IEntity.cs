using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEntity
{
    public enum DamageType
    {
        Normal,
        Dash,
        Punch,
        LavaOrSpike
    }
    
    void Damage(int numberOfTimes, DamageType damageType, Transform damagedBy);

    //int GetHealth();
}
