using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerEntity : MonoBehaviour, IEntity
{
    [SerializeField] private GameObject ibo;
    [SerializeField] private GameObject obi;

    [SerializeField] private float health;
    [SerializeField] private float hitInvincibleDuration;
    [SerializeField] private Vector2 throwForce;
    [SerializeField] private float stopTime;
    [SerializeField] private float timeAfterDeath;

    [SerializeField] private int safeTimeBack;
    
    private PlayerController _playerController;
    private Animator _iboAnimator;
    private Animator _obiAnimator;
    private Rigidbody2D _rigidbody2D;
    private SpriteRenderer _iboSpriteRenderer;
    private SpriteRenderer _obiSpriteRenderer;
    private bool _isBeingHit;
    private Vector3 _lastSafeSpace;
    
    private void Start()
    {
        _playerController = GetComponent<PlayerController>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _iboAnimator = ibo.GetComponent<Animator>();
        _obiAnimator = obi.GetComponent<Animator>();
        _iboSpriteRenderer = ibo.GetComponent<SpriteRenderer>();
        _obiSpriteRenderer = obi.GetComponent<SpriteRenderer>();
        _lastSafeSpace = transform.position;
    }

    private void Update()
    {
        RememberLastGroundedPosition();
        UpdateAnimParam();
    }

    private void UpdateAnimParam()
    {
        if (health <= 0)
        {
            _rigidbody2D.velocity = Vector2.zero;
            
            if (_playerController.GetIsIboActive())
            {
                _iboAnimator.SetBool("IsDead", true);
            }
            else
            {
                _obiAnimator.SetBool("IsDead", true);
            }
        }
        else
        {
            if (_playerController.GetIsIboActive())
            {
                _iboAnimator.SetBool("IsDead", false);
            }
            else
            {
                _obiAnimator.SetBool("IsDead", false);
            }
        }
    }

    // Remembers last safe spot SafeTimeBack
    void RememberLastGroundedPosition()
    {
        if (!_playerController.GetIsGrounded()) return;
        if (_rigidbody2D.velocity.magnitude >= 0.1f && Input.GetAxis("Horizontal") == 0) return;
        if (Time.time % safeTimeBack >= 0.1f) return;

        _lastSafeSpace = transform.position;
    }

    public void Damage(int numberOfTimes, IEntity.DamageType damageType, Transform damagedBy)
    {
        if (_isBeingHit) return;
        if (health <= 0) return;
        if (_playerController.GetIsDashing() && damagedBy.GetComponent<EnemyEntity>().CanBeDashed()) return;

        // Receive damage
        if (obi.activeSelf)
        {
            health -= numberOfTimes/2f;
        }
        else
        {
            health -= numberOfTimes;
        }
        
        _isBeingHit = true;
        Time.timeScale = 0;
        
        StartCoroutine(StopTime(damageType, damagedBy));

        if (ibo.activeSelf)
        {
            _iboAnimator.SetTrigger("HitTrigger");
        }
        else
        {
            _obiAnimator.SetTrigger("HitTrigger");
        }
        
    }

    IEnumerator Invincibility()
    {
        yield return new WaitForSeconds(hitInvincibleDuration);
        _playerController.SetCanUseInput(true);
        _isBeingHit = false;
    }

    public float GetHealth()
    {
        return health;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.CompareTag("DamageTilemap"))
        {
            Damage(1, IEntity.DamageType.LavaOrSpike, null);
        }
    }

    IEnumerator StopTime(IEntity.DamageType damageType, Transform damagedBy)
    {
        yield return new WaitForSecondsRealtime(stopTime);
        Time.timeScale = 1;
        _playerController.SetCanUseInput(false);

        if (health <= 0)
        {
            _rigidbody2D.velocity = Vector2.zero;
            if (_playerController.GetIsIboActive())
            {
                _iboAnimator.SetTrigger("DeathTrigger");
            }
            else
            {
                _obiAnimator.SetTrigger("DeathTrigger");
            }

            _playerController.enabled = false;
            _rigidbody2D.isKinematic = true;
            StartCoroutine(DeathAnim());
        }
        else
        {
            // Transform operation
            if (damageType == IEntity.DamageType.Dash || damageType == IEntity.DamageType.Punch ||
                damageType == IEntity.DamageType.Normal)
            {
                int sign = 1;
                if (damagedBy.position.x > transform.position.x)
                {
                    sign = -1;
                }
                _rigidbody2D.velocity = Vector2.right * (throwForce.x * sign) + Vector2.up * throwForce.y;
                StartCoroutine(Invincibility());
            }
            else if (damageType == IEntity.DamageType.LavaOrSpike)
            {
                transform.position = _lastSafeSpace + Vector3.up * 0.1f + Mathf.Sign(_lastSafeSpace.x - transform.position.x) * 1.0f * Vector3.right;
                _rigidbody2D.velocity = Vector2.zero;
                StartCoroutine(Invincibility());
            }
        }        
        

    }

    IEnumerator DeathAnim()
    {
        yield return new WaitForSeconds(timeAfterDeath);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

    }
}
