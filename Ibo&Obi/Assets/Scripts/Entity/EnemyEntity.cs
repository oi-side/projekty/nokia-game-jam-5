using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyEntity : MonoBehaviour, IEntity
{
    [Header("Damage/Health")]
    [SerializeField] private int health;
    [SerializeField] private float hitAnimlength;
    [SerializeField] private bool canBePunched;
    [SerializeField] private bool canBeDashed;
    
    [Space]
    [Header("Walk")]
    [SerializeField] private float walkSpeed;
    [SerializeField] private float minX;
    [SerializeField] private float maxX;

    private Rigidbody2D _rigidbody2D;
    private Animator _animator;
    private SpriteRenderer _renderer;
    private Collider2D _collider2D;
    private bool _isWalkingRight;
    private bool _isBeingHit;
    private bool _canTurn = true;
    
    // Start is called before the first frame update
    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        _renderer = GetComponent<SpriteRenderer>();
        _collider2D = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {
        if (_isBeingHit)
        {
            _rigidbody2D.velocity = Vector2.zero;
        }
        
        CheckMinMax();

        Walk();
        
    }

    private void Walk()
    {
        Vector2 velocity = _rigidbody2D.velocity;

        if (_isBeingHit)
        {
            velocity.x = 0;
        }
        else if (_isWalkingRight)
        {
            velocity.x = walkSpeed;
        }
        else
        {
            velocity.x = -walkSpeed;
        }

        _rigidbody2D.velocity = velocity;
    }

    private void CheckMinMax()
    {
        if (!_canTurn) return;
        
        if (transform.position.x <= minX)
        {
            _isWalkingRight = true;
            _renderer.flipX = true;
            _canTurn = false;
            StartCoroutine(TurnCooldown());

        }
        else if (transform.position.x >= maxX)
        {
            _isWalkingRight = false;
            _renderer.flipX = false;
            _canTurn = false;
            StartCoroutine(TurnCooldown());
        }
    }

    IEnumerator TurnCooldown()
    {
        yield return new WaitForSeconds(0.5f);
        _canTurn = true;
    }

    IEnumerator HitCooldown()
    {
        yield return new WaitForSeconds(hitAnimlength);
        _isBeingHit = false;
        _collider2D.enabled = true;
    }
    
    IEnumerator DeathCooldown()
    {
        yield return new WaitForSeconds(hitAnimlength);
        Destroy(gameObject);
    }
    
    public void Damage(int numberOfTimes, IEntity.DamageType damageType, Transform damagedBy)
    {
        if (!canBeDashed && damageType == IEntity.DamageType.Dash) return;
        if (!canBePunched && damageType == IEntity.DamageType.Punch) return;
        
        health -= numberOfTimes;
        
        _animator.SetTrigger("HitTrigger");
        _collider2D.enabled = false;
        _isBeingHit = true;

        if (health <= 0)
        {
            StartCoroutine(DeathCooldown());
        }
        else
        {
            StartCoroutine(HitCooldown());
        }
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        if (_isBeingHit) return;

        IEntity entity = null;
        if (other.collider.CompareTag("Obi") || other.collider.CompareTag("Ibo"))
        {
            entity = other.collider.transform.parent.GetComponent<IEntity>();
        }
        
        if (other.collider.CompareTag("Player"))
        {
            entity = other.collider.GetComponent<IEntity>();
        }

        if (entity != null)
        {
            entity.Damage(1, IEntity.DamageType.Normal, transform);
        }
    }

    public bool CanBePunched()
    {
        return canBePunched;
    }
    
    public bool CanBeDashed()
    {
        return canBeDashed;
    }
}
