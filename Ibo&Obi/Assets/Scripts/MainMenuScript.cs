using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{

    public GameObject newGameTxt;
    public GameObject continueGameTxt;
    public GameObject creditsScreen;
    // Start is called before the first frame update
    void Start()
    {
        if(PlayerPrefs.GetInt("AtLevelIdx", 0) > 0)
        {
            newGameTxt.SetActive(false);
            continueGameTxt.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Interact") && !creditsScreen.activeSelf)
        {
            PlayerPrefs.SetInt("AtLevelIdx", 1);
            LevelLoad(1);
        }
        if (Input.GetButtonDown("Select"))
        {
            creditsScreen.SetActive(!creditsScreen.activeSelf);
        }
        if (Input.GetButtonDown("Start") && continueGameTxt.activeSelf)
        {
            LevelLoad(PlayerPrefs.GetInt("AtLevelIdx"));
        }
    }


    public void LevelLoad(int sceneIndex)
    {
        StartCoroutine(LoadScene(sceneIndex));
    }

    IEnumerator LoadScene(int sceneIndex)
    {
        AsyncOperation loadOperation = SceneManager.LoadSceneAsync(sceneIndex);

        while (!loadOperation.isDone)
        {
            yield return null;
        }
    }

}
