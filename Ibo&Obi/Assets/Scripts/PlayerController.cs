using System;
using System.Collections;
using System.Text;
using UnityEngine;
using UnityEngine.Rendering;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private GameObject ibo;
    [SerializeField] private GameObject obi;

    [Tooltip("Size of Ibo grounded collider.")]
    [SerializeField] private float iboGroundedSizeX;
    [Tooltip("Size of Obi grounded collider.")]
    [SerializeField] private float obiGroundedSizeX;

    [Space] [Header("Speed & Switch")]
    
    [Tooltip("Manual On/Off switch")]
    [SerializeField] private bool manualOffSwitchButton;
    
    [Tooltip("Pixels Per Second.")]
    [SerializeField] private float iboSpeed;

    [Tooltip("Pixels Per Second.")]
    [SerializeField] private float obiSpeed;
    
    [Tooltip("Amount of time between switch.")]
    [SerializeField] private float iboObiSwitchCooldown;
    
    [Tooltip("Amount of time anim switch.")]
    [SerializeField] private float iboObiSwitchAnimLength;
    
    [Space]
    [Header("Physics")]

    [Tooltip("Amount of gravity applied.")]
    [SerializeField] private float gravityStrength;
    
    [Tooltip("Max iboSpeed of falling.")]
    [SerializeField] private float terminalVelocity;
    
    
    [Space]
    [Header("Jumping")]

    [Tooltip("Jump force.")]
    [SerializeField] private float jumpForceIbo;
    
    [Tooltip("Jump force.")]
    [SerializeField] private float jumpForceObi;

    [Tooltip("Size of overlap box, when it hits a jump will not be used.")]
    [SerializeField] private Vector2 overlayBoxSize;
    
    [Tooltip("Raycast offset not to hit obi or ibo.")]
    [SerializeField] private float overlayBoxYOffset;
    
    [Space]
    [Header("Special Abilities Ibo")]
    
    [Tooltip("Force of dash.")]
    [SerializeField] private float dashForce;

    [Tooltip("Time spent with dash.")]
    [SerializeField] private float dashTime;

    [Tooltip("Dash cooldown.")]
    [SerializeField] private float dashCooldown;

    [Tooltip("Time spent charging.")]
    [SerializeField] private float superJumpChargeTime;

    [Tooltip("kojot time.")]
    [SerializeField] private float superJumpAfterChargeTime;

    [Tooltip("superJump force.")]
    [SerializeField] private float superJumpForce;
    
    [Space]
    [Header("Special Abilities Obi")]
    
    [Tooltip("Punch point.")]
    [SerializeField] private Transform punchPoint;

    [Tooltip("Punch time.")]
    [SerializeField] private float punchTime;
    
    [Tooltip("Punch cooldown.")]
    [SerializeField] private float punchCooldown;

    [Tooltip("Stomp point.")]
    [SerializeField] private Transform stompPoint;
    
    [Tooltip("Stomp delay.")]
    [SerializeField] private float stompDelay;
    
    [Tooltip("Multiplier for gravity while stomping.")]
    [SerializeField] private float stompGravityMultiplier;

    [Header("Sounds")]
    
    [SerializeField] private AudioClip music;
    [SerializeField] private AudioClip punchSound;
    [SerializeField] private AudioClip stompSound;


    private bool _canSwitch = true;
    private SpriteRenderer _iboRenderer;
    private SpriteRenderer _obiRenderer;
    private Animator _iboAnimator;
    private Animator _obiAnimator;
    private float _lastHorizontalInput;
    private Rigidbody2D _rigidbody;
    private BoxCollider2D _boxCollider;
    private bool _canUseInput = true;
    private AudioSource _audioSource;
    
    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _iboRenderer = ibo.GetComponent<SpriteRenderer>();
        _iboAnimator = ibo.GetComponent<Animator>();
        _obiRenderer = obi.GetComponent<SpriteRenderer>();
        _obiAnimator = obi.GetComponent<Animator>();
        _boxCollider = GetComponent<BoxCollider2D>();
        _audioSource = GetComponent<AudioSource>();
        
        if (ibo.activeSelf)
        {
            Vector2 size = _boxCollider.size;
            size.x = iboGroundedSizeX;
            _boxCollider.size = size;
        }
        else
        {
            Vector2 size = _boxCollider.size;
            size.x = obiGroundedSizeX;
            _boxCollider.size = size;
        }
    }

    // Update is called once per frame
    void Update()
    {
        ObiIboSwitch();
        AnimatorSetters();
        SnapPositionToGrid();
        Interact();
        Sound();
    }

    private void FixedUpdate()
    {
        InputUtility();
        Crouch();
        Movement();
        Jump();
        
        if (ibo.activeSelf)
        {
            Dash();
            SuperJump();
        }
        else
        {
            Punch();
            Stomp();
        }
        
        RigidbodyPhysics();
    }

    // --------------------------------------Crouch---------------------------------------
    private bool _isCrouching;
    void Crouch()
    {
        bool _isCrouchingBackup = _isCrouching;     
        
        _isCrouching = false;
        if (!_canUseInput) return;
        if (!_isGrounded) return;
        if (_isDashingForce) return;
        if (ibo.activeSelf && _iboAnimator.GetCurrentAnimatorStateInfo(0).IsName("Ibo_Hit")) return;
        if (obi.activeSelf && _obiAnimator.GetCurrentAnimatorStateInfo(0).IsName("Obi_Hit")) return;
        if (Input.GetAxis("Vertical") >= 0) return;

        Vector2 velocity = _rigidbody.velocity;
        velocity.x = 0;
        _rigidbody.velocity = velocity;
        _isCrouching = true;
        
        // Super jump charge
        if (ibo.activeSelf)
        {
            if (_isCrouchingBackup == false)
            {
                _startChargeTime = Time.time;
            }
        }
    }
    
    // --------------------------------------Movement-------------------------------------
    void Movement()
    {
        if (!_canUseInput) return;
        if (_isCrouching) return;
        if (_isDashingForce) return;
        if (_isPunching) return;
        if (ibo.activeSelf && _iboAnimator.GetCurrentAnimatorStateInfo(0).IsName("Ibo_Hit")) return;
        if (obi.activeSelf && _obiAnimator.GetCurrentAnimatorStateInfo(0).IsName("Obi_Hit")) return;
        
        float horizontalInput = Input.GetAxis("Horizontal");
        Vector2 velocity = _rigidbody.velocity;

        // Choose speed of ibo or obi
        float speed;
        if (ibo.activeSelf)
        {
            speed = iboSpeed;
        }
        else
        {
            speed = obiSpeed;
        }
        
        if (horizontalInput > 0.005)
        {
            velocity.x = speed;
        }
        else if (horizontalInput < -0.005)
        {
            velocity.x = -speed;
        }
        else
        {
            velocity.x = 0;
        }

        _rigidbody.velocity = velocity;
    }

    // -----------------------------------Obi and Ibo Switch------------------------------------------
    private bool _isSwitching;
    void ObiIboSwitch()
    {
        if (manualOffSwitchButton) return;
        if (!_canUseInput) return;
        if (!_canSwitch) return;
        if (Input.GetAxis("IboObiSwitch") <= 0) return;
        
        if (ibo.activeSelf)
        {
            Vector2 position = punchPoint.position + Vector3.down * 0.4f;
            Collider2D hit1 = Physics2D.OverlapPoint(position);
            bool hit1Collision = false;
            if (hit1 != null)
            {
                hit1Collision = hit1.CompareTag("Ground");
            }
            
            position.x -= punchPoint.localPosition.x * 2;
            Collider2D hit2 = Physics2D.OverlapPoint(position);
            bool hit2Collision = false;
            if (hit2 != null)
            {
                hit2Collision = hit2.CompareTag("Ground");
            }

            if (hit1Collision && hit2Collision) return;
            
            _iboAnimator.SetTrigger("SwitchTrigger");
        }
        else
        {
            _obiAnimator.SetTrigger("SwitchTrigger");
        }
        
        _canSwitch = false;
        _isSwitching = true;
        StartCoroutine(SwitchAnim());
        StartCoroutine(SwitchCooldown());
    }

    IEnumerator SwitchAnim()
    {
        yield return new WaitForSeconds(iboObiSwitchAnimLength);
        
        if (ibo.activeSelf)
        {
            Vector2 size = _boxCollider.size;
            size.x = obiGroundedSizeX;
            _boxCollider.size = size;
                
            ibo.SetActive(false);
            obi.SetActive(true);
        }
        else
        {
            Vector2 size = _boxCollider.size;
            size.x = iboGroundedSizeX;
            _boxCollider.size = size;
                
            ibo.SetActive(true);
            obi.SetActive(false);
        }
        
        _isSwitching = false;
    }

    IEnumerator SwitchCooldown()
    {
        yield return new WaitForSeconds(iboObiSwitchCooldown);
        _canSwitch = true;
    }

    // ------------------------------Animation Setter-------------------------------------------------
    void AnimatorSetters()
    {
        bool isIboActive = ibo.activeSelf;
        
        // Turn Trigger and Walk Bool
        if (_rigidbody.velocity.x > 1 || _rigidbody.velocity.x < -1)
        {
            // Turn Trigger
            if (Mathf.Sign(_lastHorizontalInput) != Mathf.Sign(_rigidbody.velocity.x))
            {
                if (isIboActive)
                {
                    _iboAnimator.SetTrigger("TurnTrigger");
                }
                else
                {
                    _obiAnimator.SetTrigger("TurnTrigger");
                }
            }
            else
            {
                if (!_isCrouching)
                {
                    if (isIboActive)_iboAnimator.SetBool("IsWalking", true);
                    else _obiAnimator.SetBool("IsWalking", true);    
                }
                else
                {
                    if (isIboActive)_iboAnimator.SetBool("IsWalking", false);
                    else _obiAnimator.SetBool("IsWalking", false);
                }
                
            }

            if (!_isDashingForce && !_isPunching)
            {
                if (_rigidbody.velocity.x > 0)
                {
                    _obiRenderer.flipX = false;
                    _iboRenderer.flipX = false;
                    Vector3 punchPos = punchPoint.localPosition;
                    punchPos.x = Mathf.Abs(punchPos.x);
                    punchPoint.localPosition = punchPos;
                }
                else
                {
                    _obiRenderer.flipX = true;
                    _iboRenderer.flipX = true;
                    Vector3 punchPos = punchPoint.localPosition;
                    punchPos.x = -Mathf.Abs(punchPos.x);
                    punchPoint.localPosition = punchPos;
                }
            }
            
            _lastHorizontalInput = _rigidbody.velocity.x;
        }
        else
        {
            if (isIboActive)_iboAnimator.SetBool("IsWalking", false);
            else _obiAnimator.SetBool("IsWalking", false);
        }
        
        // Crouch
        if(isIboActive) _iboAnimator.SetBool("IsCrouching", _isCrouching);
        else _obiAnimator.SetBool("IsCrouching", _isCrouching);

        // Velocity
        Vector2 velocity = _rigidbody.velocity;
        if (isIboActive) _iboAnimator.SetFloat("VelocityY", velocity.y);
        else _obiAnimator.SetFloat("VelocityY", velocity.y);
        
        // Dash and superjump
        if (isIboActive)
        {
            _iboAnimator.SetBool("IsDashing", _isDashingForce);
            _iboAnimator.SetBool("IsSuperJumping", _isSuperJumping);
        }

        // Grounded
        if (isIboActive) _iboAnimator.SetBool("IsGrounded", _isGrounded);
        else _obiAnimator.SetBool("IsGrounded", _isGrounded);
        
        // Stomp
        if (!isIboActive) _obiAnimator.SetBool("IsStomping", _isStomping);
        
        // Switch
        if(!isIboActive) _obiAnimator.SetBool("IsSwitching", _isSwitching);

    }

    // --------------------------------Utility---------------------------------------------------------
    void SnapPositionToGrid()
    {
        /*transform.position = new Vector2(Mathf.Round( transform.position.x * 100) / 100, 
                                           Mathf.Round( (transform.position.y) * 100) / 100);*/
    }

    private bool _hasLetGoOfJump = true;
    void InputUtility()
    {
        if (!_canUseInput) return;

        if (Input.GetAxis("Vertical") < 0.8f)
        {
            _hasLetGoOfJump = true;
        }
    }

    // -------------------------------Jump------------------------------------------------------------
    private bool _canJump;
    void Jump()
    {
        if (!_canUseInput) return;
        if (_isCrouching) return;
        if (_isPunching) return;
        if (!_canJump) return;
        if (!_isGrounded) return;
        if (!_hasLetGoOfJump) return;
        if (ibo.activeSelf && _iboAnimator.GetCurrentAnimatorStateInfo(0).IsName("Ibo_Hit")) return;
        if (obi.activeSelf && _obiAnimator.GetCurrentAnimatorStateInfo(0).IsName("Obi_Hit")) return;
        if (Input.GetAxis("Vertical") <= 0) return;

        if (ibo.activeSelf)
        {
            Collider2D hit = Physics2D.OverlapBox(transform.position + Vector3.up * overlayBoxYOffset, overlayBoxSize, 0);
            if (hit)
            {
                IboNope();
                return;
            }
        }
        
        // Jump Trigger
        if(ibo.activeSelf) _iboAnimator.SetTrigger("JumpTrigger");
        else _obiAnimator.SetTrigger("JumpTrigger");
        
        _canJump = false;
        _hasLetGoOfJump = false;
        if (ibo.activeSelf)
        {
            if (!_isSuperJumping)
            {
                Vector2 velocity = _rigidbody.velocity;
                velocity.y = jumpForceIbo;
                _rigidbody.velocity = velocity;
            }
            else
            {
                _iboAnimator.SetTrigger("SuperJumpTrigger");
                Vector2 velocity = _rigidbody.velocity;
                velocity.y = superJumpForce;
                _rigidbody.velocity = velocity;
                _isSuperJumping = false;
            }
            
        }
        else
        {
            Vector2 velocity = _rigidbody.velocity;
            velocity.y = jumpForceObi;
            _rigidbody.velocity = velocity;
        }
    }

    // ------------------------------Dash----------------------------------------------
    private bool _isDashingForce;
    private bool _isDashingCooldown;
    private bool _canDash = true;
    void Dash()
    {
        if (!_canUseInput) return;
        if (Input.GetAxis("SpecialAbility") <= 0) return;
        if (!_canDash) return;
        if (_isCrouching) return;
        if (ibo.activeSelf && _iboAnimator.GetCurrentAnimatorStateInfo(0).IsName("Ibo_Hit")) return;
        if (obi.activeSelf && _obiAnimator.GetCurrentAnimatorStateInfo(0).IsName("Obi_Hit")) return;
        
        _isDashingForce = true;
        _isDashingCooldown = true;
        _canDash = false;
        _iboAnimator.SetTrigger("DashTrigger");
        
        Vector2 velocity = _rigidbody.velocity;
        
        if (_iboRenderer.flipX)
        {
            velocity.x = -dashForce;
        }
        else
        {
            velocity.x = dashForce;
        }

        velocity.y = 0;
        _rigidbody.velocity = velocity;

        StartCoroutine(Dashing());
    }

    IEnumerator Dashing()
    {
        yield return new WaitForSeconds(dashTime);
        _isDashingForce = false;
        StartCoroutine(DashCooldown());
    }
    
    IEnumerator DashCooldown()
    {
        yield return new WaitForSeconds(dashCooldown);
        if (_isGrounded) _canDash = true;
        _isDashingCooldown = false;
    }
    
    // ------------------------------SuperJump----------------------------------------------
    private float _startChargeTime;
    private bool _isSuperJumping;
    private void SuperJump()
    {
        if (!_canUseInput) return;

        if (!_isCrouching) return;
        if (Time.time - _startChargeTime < superJumpChargeTime) return;
        if (_isSuperJumping) return;        

        StartCoroutine(CheckForJumpAfterCharge());
    }

    IEnumerator CheckForJumpAfterCharge()
    {
        _isSuperJumping = true;
        yield return new WaitForSeconds(superJumpAfterChargeTime);
        _isSuperJumping = false;
    }
    
    // -------------------------------Punch--------------------------------------------
    private bool _canPunch = true;
    private bool _isPunching;
    void Punch()
    {
        if (!_canUseInput) return;
        if (ibo.activeSelf && _iboAnimator.GetCurrentAnimatorStateInfo(0).IsName("Ibo_Hit")) return;
        if (obi.activeSelf && _obiAnimator.GetCurrentAnimatorStateInfo(0).IsName("Obi_Hit")) return;
        if (Input.GetAxis("SpecialAbility") <= 0) return;
        if (!_isGrounded) return;
        if (!_canPunch) return;

        Vector2 velocity = Vector2.zero;
        _rigidbody.velocity = velocity;
        _canPunch = false;
        _obiAnimator.SetTrigger("PunchTrigger");
        _isPunching = true;

        // Punch forward upper block
        Collider2D other = Physics2D.OverlapPoint(punchPoint.position + Vector3.up * 0.4f);
        if (other != null)
        {
            BreakableTileMap breakableTileMap = other.GetComponent<BreakableTileMap>();
            if (breakableTileMap != null)
            {
                breakableTileMap.DestroyTile(punchPoint.position.x, punchPoint.position.y + 0.4f, true);
                _punchTrigger = true;
            }
            
            // Damage Enemy by punch
            if (other.CompareTag("Enemy"))
            {
                other.GetComponent<EnemyEntity>().Damage(1, IEntity.DamageType.Punch, transform);
                _punchTrigger = true;
            }
        }

             
        // Punch forward lower block
        other = Physics2D.OverlapPoint(punchPoint.position + Vector3.down * 0.4f);
        if (other != null)
        {
            BreakableTileMap breakableTileMap = other.GetComponent<BreakableTileMap>();
            if (breakableTileMap != null)
            {
                breakableTileMap.DestroyTile(punchPoint.position.x, punchPoint.position.y - 0.4f, true);
                _punchTrigger = true;
            }
            
            // Damage Enemy by punch
            if (other.CompareTag("Enemy"))
            {
                other.GetComponent<EnemyEntity>().Damage(1, IEntity.DamageType.Punch, transform);
                _punchTrigger = true;
            }
        }

        StartCoroutine(Punching());
    }

    IEnumerator Punching()
    {
        yield return new WaitForSeconds(punchTime);
        StartCoroutine(PunchCooldown());
        _isPunching = false;
    }

    IEnumerator PunchCooldown()
    {
        yield return new WaitForSeconds(punchCooldown);
        _canPunch = true;
    }
    
    // ---------------------------Stomp/Pound-----------------------------------------
    private bool _isStomping;
    private Coroutine _stompCoroutine;
    void Stomp()
    {
        if (!_canUseInput) return;
        if(_isGrounded) return;
        if (Input.GetAxis("Vertical") >= 0f) return;
        if (ibo.activeSelf && _iboAnimator.GetCurrentAnimatorStateInfo(0).IsName("Ibo_Hit")) return;
        if (obi.activeSelf && _obiAnimator.GetCurrentAnimatorStateInfo(0).IsName("Obi_Hit")) return;
        
        _isStomping = true;
        _stompTrigger = true;

        if (_stompCoroutine != null) StopCoroutine(_stompCoroutine);
        _stompCoroutine = StartCoroutine(Stomping());
    }

    IEnumerator Stomping()
    {
        yield return new WaitForSeconds(stompDelay);
        Debug.Log("Stomp stop");
        _isStomping = false;
    }

    // ----------------------------------Physics--------------------------------------
    private bool _isGrounded;
    
    void RigidbodyPhysics()
    {
        if (ibo.activeSelf && _iboAnimator.GetCurrentAnimatorStateInfo(0).IsName("Ibo_Hit")) return;
        if (obi.activeSelf && _obiAnimator.GetCurrentAnimatorStateInfo(0).IsName("Obi_Hit")) return;
        Vector2 velocity = _rigidbody.velocity;
        
        // While dashing we do not fall
        if (_isDashingForce)
        {
            velocity.y = 0;
        }

        // While stomping the gravity is stronger
        if (_isStomping)
        {
            _rigidbody.gravityScale = stompGravityMultiplier;
        }
        else
        {
            _rigidbody.gravityScale = gravityStrength;
        }

        // Terminal velocity
        if (velocity.y < -terminalVelocity) velocity.y = -terminalVelocity;
        
        _rigidbody.velocity = velocity;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Ground"))
        {
            _isGrounded = true;
            _canJump = true;
            if (_isStomping)
            {
                other = Physics2D.OverlapPoint(stompPoint.position + Vector3.right * 0.4f);
                if (other != null)
                {
                    BreakableTileMap breakableTileMap = other.GetComponent<BreakableTileMap>();
                    if (breakableTileMap != null)
                    {
                        breakableTileMap.DestroyTile(stompPoint.position.x + 0.4f, stompPoint.position.y, true);
                    }
                }
                
                other = Physics2D.OverlapPoint(stompPoint.position + Vector3.left * 0.4f);
                if (other != null)
                {
                    BreakableTileMap breakableTileMap = other.GetComponent<BreakableTileMap>();
                    if (breakableTileMap != null)
                    {
                        breakableTileMap.DestroyTile(stompPoint.position.x - 0.4f, stompPoint.position.y, true);
                    }
                }
            }

            if (!_canDash) _canDash = true;
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Ground"))
        {
            if (!_canDash && !_isDashingCooldown) _canDash = true;
            _isGrounded = true;
            _canJump = true;
        }
    }
    
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Ground"))
        {
            _isGrounded = false;
            // TODO TEST
            //_canJump = false;
        }
    }
    
    //--------------------------Interact----------------------------------
    void Interact()
    {
        if (!_canUseInput) return;
        if (!_isGrounded) return;
        if (Input.GetAxis("Interact") <= 0.8f) return;

        Collider2D[] collider2Ds = Physics2D.OverlapCircleAll(transform.position - Vector3.up * 0.5f, 1);
        foreach (var collider in collider2Ds)
        {
            Lever lever = collider.GetComponent<Lever>();
            if (lever != null)
            {
                if (obi.activeSelf)
                {
                    ObiNope();
                }
                else
                {
                    lever.Interact();
                }
            }
        }
    }

    //--------------------------------Dash-Damage--------------------------------
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (!_isDashingForce) return;
        if (ibo.activeSelf && _iboAnimator.GetCurrentAnimatorStateInfo(0).IsName("Ibo_Hit")) return;
        if (obi.activeSelf && _obiAnimator.GetCurrentAnimatorStateInfo(0).IsName("Obi_Hit")) return;
        
        if (other.collider.CompareTag("Enemy"))
        {
            other.collider.GetComponent<EnemyEntity>().Damage(1, IEntity.DamageType.Dash, transform);
        }

        _rigidbody.velocity = _rigidbody.velocity.normalized * dashForce;
    }
    
    // -----------------------------Nope-Anim----------------------------------
    public void IboNope()
    {
        if (obi.activeSelf) return;
        _iboAnimator.SetTrigger("NopeTrigger");
    }

    public void ObiNope()
    {
        if (ibo.activeSelf) return;
        _obiAnimator.SetTrigger("NopeTrigger");
    }
    
    //-------------------------------Sounds----------------------------------
    private bool _stompTrigger;
    private bool _punchTrigger;
    private float _tempTime;

    private void Sound()
    {
        if (_punchTrigger)
        {
            if (_audioSource.clip == music)
            {
                _tempTime = _audioSource.time;
            }

            _audioSource.time = 0;
            _audioSource.clip = punchSound;
            _audioSource.loop = false;
            _punchTrigger = false;
            _audioSource.Play();
        }
        
        else if (_stompTrigger)
        {
            if (_audioSource.clip == music)
            {
                _tempTime = _audioSource.time;
            }

            _audioSource.time = 0;
            _audioSource.clip = stompSound;
            _audioSource.loop = false;
            _stompTrigger = false;
            _audioSource.Play();
        }

        if (!_audioSource.isPlaying)
        {
            _audioSource.clip = music;
            _audioSource.loop = true;
            _audioSource.time = Mathf.Min(_tempTime, music.length - 0.01f);
            _audioSource.Play();
        }

        if (_audioSource.clip == music)
        {
            _tempTime = _audioSource.time;
        }
    }


    public bool GetIsDashing()
    {
        return _isDashingForce;
    }

    public bool GetIsGrounded()
    {
        return _isGrounded;
    }

    public void SetCanUseInput(bool canUseInput)
    {
        _canUseInput = canUseInput;
    }

    public bool GetIsIboActive()
    {
        return ibo.activeSelf;
    }

    public void SetMusic(AudioClip newMusic)
    {
        music = newMusic;
        _audioSource.clip = music;
        _audioSource.time = 0;
        _audioSource.Play();
    }
}
