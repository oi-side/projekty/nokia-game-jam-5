using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockBreakDestroy : MonoBehaviour
{
    [SerializeField] private float timeToDestroy;
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DestroySelf());
    }

    IEnumerator DestroySelf()
    {
        yield return new WaitForSeconds(timeToDestroy);
        Destroy(gameObject);
    }
}
