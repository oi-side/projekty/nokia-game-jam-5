using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Tilemaps;

public class BreakableTileMap : MonoBehaviour
{
    private Tilemap _destructibleTilemap;

    [SerializeField] private GameObject[] punchableTilesGameObjects;
    [SerializeField] private GameObject[] otherTilesGameObjects;

    private void Start()
    {
        _destructibleTilemap = GetComponent<Tilemap>();
    }

    public void DestroyTile(float x, float y, bool destroyingFromPunchOrStomp)
    {
        Vector3Int tileXY = _destructibleTilemap.WorldToCell(new Vector3(x, y));

        // Look if tile exists or if we can place an animated tile there
        Sprite baseSprite = _destructibleTilemap.GetSprite(tileXY);
        if (baseSprite == null)
        {
            return;
        }
        if (punchableTilesGameObjects.Length < 1 || otherTilesGameObjects.Length < 1) return;

        GameObject gameObjToInstantiate = null;
        
        // Look for sprites that match the current cell in punchable
        if (destroyingFromPunchOrStomp)
        {
            gameObjToInstantiate = LookInPunchable(baseSprite);
        }

        if (gameObjToInstantiate == null)
        {
            gameObjToInstantiate = LookInOther(baseSprite);
        }

        // if found destroy sprite and instantiate a gameobject
        if (gameObjToInstantiate != null)
        {
            _destructibleTilemap.SetTile(tileXY, null);
            GameObject instance = Instantiate(gameObjToInstantiate);
            instance.transform.position = (Vector2)_destructibleTilemap.CellToWorld(tileXY) + Vector2.one * 0.5f;

            // Check if the summoned tile is of type DoorOpen
            DoorOpen doorOpen = instance.GetComponent<DoorOpen>();
            if (doorOpen != null)
            {
                doorOpen.SetTilemap(_destructibleTilemap);
            }
        }
        else
        {
            // Didnt hit known custom tile
        }
        
    }

    GameObject LookInPunchable(Sprite baseSprite)
    {
        for (int i = 0; i < punchableTilesGameObjects.Length; i++)
        {
            Sprite animSpriteFirst = punchableTilesGameObjects[i].GetComponent<SpriteRenderer>().sprite;
            if (animSpriteFirst == baseSprite)
            {
                return punchableTilesGameObjects[i];
            }
        }

        return null;
    }
    
    GameObject LookInOther(Sprite baseSprite)
    {
        for (int i = 0; i < otherTilesGameObjects.Length; i++)
        {
            Sprite animSpriteFirst = otherTilesGameObjects[i].GetComponent<SpriteRenderer>().sprite;
            if (animSpriteFirst == baseSprite)
            {
                return otherTilesGameObjects[i];
            }
        }

        return null;
    }

}
