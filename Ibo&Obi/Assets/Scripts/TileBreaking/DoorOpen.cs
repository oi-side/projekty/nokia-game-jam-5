using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class DoorOpen : MonoBehaviour
{
    [SerializeField] private float timeToOpen;
    [SerializeField] private Tilemap tilemap;
    [SerializeField] private Sprite[] spritesToConsiderSame;

    [Space] [Header("TESTING")]
    [SerializeField] public bool debug;
    [SerializeField] public bool start;
    
    // Start is called before the first frame update
    void Start()
    {
        if (!debug) StartCoroutine(WaitTillOpen());
        else
        {
            GetComponent<Animator>().enabled = false;
        }
    }
    
    //TESTING
    private void Update()
    {
        if (debug && start)
        {
            start = false;
            GetComponent<Animator>().enabled = true;
            StartCoroutine(WaitTillOpen());
        }
    }

    public void SetTilemap(Tilemap newTilemap)
    {
        tilemap = newTilemap;
    }

    IEnumerator WaitTillOpen()
    {
        yield return new WaitForSeconds(timeToOpen);

        Vector2 position = transform.position;
        
        Vector2 tempPosition = position + Vector2.up;
        if (CheckForSameSprite(tempPosition))
        {
            GameObject instance = DestroyTileCreateGameObject(tempPosition);
            instance.GetComponent<SpriteRenderer>().flipY = false;
        }

        tempPosition = position + Vector2.down;
        if (CheckForSameSprite(tempPosition))
        {
            GameObject instance2 = DestroyTileCreateGameObject(tempPosition);
            instance2.GetComponent<SpriteRenderer>().flipY = true;
        }
        
        Destroy(gameObject);
    }

    public bool CheckForSameSprite(Vector2 position)
    {
        Sprite sprite = tilemap.GetSprite(tilemap.WorldToCell(position));
        if (sprite == null) return false;
        
        foreach (Sprite tempSprite in spritesToConsiderSame)
        {
            if (sprite == tempSprite)
            {
                return true;
            }
        }
        
        return false;
    }
    
    public GameObject DestroyTileCreateGameObject(Vector2 position)
    {
        // DESTROY TILE
        tilemap.SetTile(tilemap.WorldToCell(position), null);

        // TESTING
        debug = false;
        start = false;
        
        // SUMMON this
        GameObject instance = Instantiate(gameObject);
        
        // location
        Vector3Int tileXY = tilemap.WorldToCell(position);
        instance.transform.position = (Vector2)tilemap.CellToWorld(tileXY) + Vector2.one * 0.5f;

        return instance;
    }
}
