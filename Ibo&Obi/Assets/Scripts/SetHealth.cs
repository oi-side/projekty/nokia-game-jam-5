using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetHealth : MonoBehaviour
{
    [SerializeField] private PlayerEntity playerEntity;
    [SerializeField] private Sprite heart;
    [SerializeField] private Sprite halfHeart;
    [SerializeField] private Sprite noHeart;
    
    [SerializeField] private Image firstHeart;
    [SerializeField] private Image secondHeart;
    [SerializeField] private Image thirdHeart;

    // Update is called once per frame
    void Update()
    {
        float health = playerEntity.GetHealth();

        // 3 hearths
        if (health >= 2.9f)
        {
            firstHeart.sprite = heart;
            secondHeart.sprite = heart;
            thirdHeart.sprite = heart;
        }
        
        // 2.5 hearths
        else if (health >= 2.4f)
        {
            firstHeart.sprite = heart;
            secondHeart.sprite = heart;
            thirdHeart.sprite = halfHeart;
        }
        
        // 2 hearths
        else if (health >= 1.9f)
        {
            firstHeart.sprite = heart;
            secondHeart.sprite = heart;
            thirdHeart.sprite = noHeart;
        }
        
        // 1.5 hearths
        else if (health >= 1.4f)
        {
            firstHeart.sprite = heart;
            secondHeart.sprite = halfHeart;
            thirdHeart.sprite = noHeart;
        }
        
        // 1 hearth
        else if (health >= 0.9f)
        {
            firstHeart.sprite = heart;
            secondHeart.sprite = noHeart;
            thirdHeart.sprite = noHeart;

        }
        
        // 0.5 hearths
        else if (health >= 0.4f)
        {
            firstHeart.sprite = halfHeart;
            secondHeart.sprite = noHeart;
            thirdHeart.sprite = noHeart;
        }

        // 0 hearths
        else
        {
            firstHeart.sprite = noHeart;
            secondHeart.sprite = noHeart;
            thirdHeart.sprite = noHeart;
        }
    }
}
