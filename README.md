# Obi-Ibo
*2D plošinovka*

Hra byla vytvořená pro [Nokia Game Jam 5](https://itch.io/jam/nokiajam5).

Tým byl složený ze tří členů oiSIDE: Jiří Kropáč, Ondřej Kyzr a Jakub Sakař.

Hra je hratelná na [itch.io](https://oiside-fee-ctu-in-prague.itch.io/obiibo).

Hráč se ujme role malého robůtka s dvěma konfiguracemi, mezi kterými musí střídat, aby se dostal z tajuplné planety.

[Walkthrough](https://youtu.be/zukDl-fLFfE?si=vsMBB6aJzXfcO8uP)
